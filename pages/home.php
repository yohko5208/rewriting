<?php
    require('../inc/connectpdo.php');
    require('../inc/functions.php');
    require('../inc/Player.php');
    require('../inc/Team.php');
    $listeTeam=findTeams();
    $home=1;
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/style/images/dota2.ico">
    <title>DOTAPEDIA - Everything you need to know on Dota 2 pro teams</title>
    <!-- Bootstrap core CSS -->
    <link href="../assets/style/css/style.css" rel="stylesheet">
    <link href="../assets/style/css/color/blue.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="assets/../assets/style/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>



<!-- Start Loading -->

<section class="loading-overlay">
    <div class="Loading-Page">
        <h2 class="loader">Loading...</h2>
    </div>
</section>

<!-- End Loading  -->

<div class="content-wrap">
    <div id="home" class="body-wrapper">
        <?php include('../inc/parallax-background.php');?>
        <?php include('../inc/navhead.php');?>

        <!-- End Section Header style3 -->

        <!-- Start Welcome To Yamen -->

        <section id="Welcome" class="light-section">
            <div class="container inner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section text-center">
                            <h1>Welcome to <strong>DOTAPEDIA</strong></h1>
                        </div>
                        <div class="description-welcome text-center">
                            <p>This is a website which gives all the information that you should know about the most popular
                                <strong>Dota 2 professional teams</strong> from all around the world!</p>
                            <p>You'll know about their current roster and their past achievements. So let's get started!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End Welcome To Yamen -->

        <!-- Start About Us -->

        <section id="about" class="dark-section">
            <div class="container inner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section text-center">
                            <h3>List of all teams</h3>
                            <div class="line-break"></div>
                        </div>
                        <div class="description-section text-center">
                            <p>Here is the alphabetical list of all the most popular <strong>Dota 2 pro teams</strong>  in the world.</p>
                        </div>
                    </div>
                </div>
                <?php foreach($listeTeam as $team){?>
                    <div class="col-md-2">
                        <a href="team-<?php echo $team->get_id(); ?>-<?php echo $team->get_Url(); ?>">
                            <div class="Team-Block">
                                <div class="Top-Img-Team">
                                    <img src="../assets/style/images/team-logo/<?php echo $team->get_Image();?>" alt="Team <?php echo $team->get_Name();?>" width="211" height="211">
                                </div>
                                <div class="Title-TeamBlock">
                                    <h4><?php echo $team->get_Name();?></h4>
                                    <p><?php echo findRegionById($team->get_Region());?></p>
                                </div>
                            </div>
                        </a>

                    </div>
                <?php }
                ?>

            </div>
        </section>

        <!-- End About Us -->

    </div>

    <!-- Back to top Link -->
    <div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
</div>


<!-- Placed at the end of the document so the pages load faster -->

<?php include('../inc/script.php');?>
</body>
</html>
