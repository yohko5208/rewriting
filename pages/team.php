<?php
require('../inc/connectpdo.php');
require('../inc/functions.php');
require('../inc/Player.php');
require('../inc/Team.php');
require('../inc/TeamImages.php');
$team = findTeamsById($_GET['id']);
$players=findPlayersByTeam($team->get_id());
$images = findImagesByTeam($team->get_id());
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10">
    <meta name="description" content="<?php echo $team->get_Overview(); ?>">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../assets/style/images/dota2.ico">
    <title><?php echo $team->get_Name(); ?> (<?php echo findRegionAbbrById($team->get_region()); ?>) : DOTAPEDIA</title>
    <!-- Bootstrap core CSS -->
    <link href="../assets/style/css/style.css" rel="stylesheet">
    <link href="../assets/style/css/color/blue.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="assets/../assets/style/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>



<!-- Start Loading -->	

<section class="loading-overlay">
    <div class="Loading-Page">
		<h2 class="loader">Loading...</h2>
    </div>
</section>	

 <!-- End Loading  -->	

<?php include('../inc/navhead.php');?>
<!-- Start Pages Title Style1 -->
<!-- End Pages Title Style1 -->

<!-- Start About Us -->

<section id="About-Us" class="light-section">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h1><?php echo $team->get_Name(); ?></h1>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p><?php echo $team->get_Overview(); ?></p>
                </div>
            </div>
        </div>
        <div class="divcod20"></div>
        <div class="row">
			<div class="col-md-6">
				<div id="owl-about-img" class="owl-carousel">
                    <?php foreach($images as $image){ ?>
                        <div><img src="../assets/style/images/team/<?php echo $image->get_Source(); ?>" alt="<?php echo $image->get_Alt(); ?>" width="570" height="250"></div>
                    <?php } ?>
				</div>
            </div>
            
            <div class="col-md-6">
            	<div class="Text-About">
                	<p><?php echo $team->get_Description(); ?></p>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- End About Us -->

<!-- Start Our Team -->

<section id="Our-Team" class="dark-section">
	<div class="container inner">
    	<div class="row">
        	<div class="col-md-12">
                <div class="title-section text-center">
                    <h3>Team Roster</h3>
                    <div class="line-break"></div>
                </div>
                <div class="description-section text-center">
                    <p>Here are the persons who part of this team.</p>
                </div>
            </div>
        </div>
        <div class="divcod30"></div>
        <div class="row">
            <?php foreach($players as $player){
                ?>
                <div class="col-md-2 col-md-push-1">
                    <div class="Team-block">
                        <div class="Top-Img">
                            <img src="../assets/style/images/players/<?php echo $player->get_Image(); ?>" alt="<?php echo $team->get_Name(); ?>-<?php echo $player->get_Nickname(); ?>" width="211" height="211" >
                        </div>

                        <div class="Title-Team text-center">
                            <h2><?php echo $player->get_Nickname(); ?> (<?php echo $player->get_Role(); ?>)</h2>
                            <p><?php echo $player->get_Name(); ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>


			
        </div>
    </div>
</section>

<!-- End Our Team -->


<?php include('../inc/footer.php'); ?>

</div>
    
<!-- Back to top Link -->
	<div id="to-top" class="main-bg"><span class="fa fa-chevron-up"></span></div>
</div>	


<!-- Placed at the end of the document so the pages load faster -->
<?php include('../inc/script.php'); ?>
</body>


</html>
