create database dotapedia;
use dotapedia;

create table regions(
  id int UNSIGNED NOT NULL AUTO_INCREMENT primary key,
  name varchar(50),
  short varchar(10)
)engine=InnoDB;

create table teams (
  id int UNSIGNED NOT NULL AUTO_INCREMENT primary key,
  name varchar(50),
  url varchar(50),
  image varchar(50),
  idRegion int(10) unsigned,
  overview varchar(250),
  description varchar(5000),
  foreign key (idRegion) references regions(id)
)engine=InnoDB;

create table players (
  id int UNSIGNED NOT NULL AUTO_INCREMENT primary key,
  name varchar(50),
  nickname varchar(50),
  image varchar(50),
  idTeam int(10) unsigned,
  role varchar(50),
  foreign key (idTeam) references teams(id)
)engine=InnoDB;

create table teamImages (
  id int UNSIGNED NOT NULL AUTO_INCREMENT primary key,
  source varchar(50),
  alt varchar(50),
  idTeam int(10) unsigned,
  foreign key (idTeam) references teams(id)
)engine=InnoDB;

insert into regions values ('','North America', 'NA');
insert into regions values ('', 'South America', 'SA');
insert into regions values ('','Europe', 'EU');
insert into regions values ('','CommonWealth of Independent States', 'CIS');
insert into regions values ('', 'China', 'CN');
insert into regions values ('','South East Asia', 'SEA');

insert into teams values('', 'Evil Geniuses', 'evil-geniuses', 'Evil-Geniuses.png', 1, '<strong>Evil Geniuses</strong>, often abbreviated <strong>EG</strong>, is one of the oldest North American professional gaming organizations. Ever since the foundation in 1999, Evil Geniuses is known to have highly successful players in every competitive game they feature and is by many considered one of the premier gaming organizations with some of the biggest names in eSports signed.', 'After the International 2011, Evil Geniuses announced their new Dota 2 team in October 2011. This roster included Clinton "Fear" Loomis, Rasmus Berth "MiSeRy" Filipsen, Jimmy "DeMoN" Ho, Amel "PlaymatE" Barudzija, and Pers Anders Olsson "Pajkatt" Lille.EG acquired Kurtis "Aui_2000" Ling from C9 in December 2014. On August 8, 2015, Evil Geniuses beat CDEC Gaming to win The International 2015, securing a first-time championship, winning a total of $6,616,014, which was one of the largest purses ever awarded in eSports and becoming the first American team to win the event. </br> On August 14, Aui_2000 was kicked off of the team. Former member Artour "Arteezy" Babaev replaced him on the roster. EG finished third at both the Frankfurt Major 2015 and the Shanghai Major 2016. On March 22, Arteezy and UNiVeRsE left the team to join Team Secret. On March 25, EG announced that former members Aui_2000 and Bulba would rejoin the roster to fill these vacancies. EG placed third at The International 2016. After the tournament the team released ppd and Fear, who both decided to retire from playing Dota 2 to pursue other opportunities within the organization. They were replaced by Andreas Franck "Cr1t-" Nielsen, who would go on to become the new captain, and Arteezy. Following their elimination at The International 2017 in August, Ludwig "zai" Wåhlberg departed Evil Geniuses saying he would seek new options about his career. Shortley after, Peter "ppd" Dager stepped down as CEO and was replaced by former team manager, Philip Aram. </br> The following month, Evil Geniuses announced that Clinton "Fear" Loomis would come out of his coaching role to rejoin the team as an active player. In late December 2017, EG announced that they would be releasing UNiVeRsE and their head coach, SVG. They would pick up Rasmus "MiSeRy" Filipsen and Sam "BuLba" Sosale to replace them, respectively.');
insert into teams values('', 'Complexity Gaming', 'complexity-gaming','Complexity.png', 1, '<strong>compLexity Gaming</strong>, abbreviated as <strong>coL</strong>, is one of the most long-standing and respected eSports teams in North America. The organization, founded in 2003 by Jason "1" Lake, has a long history of championship caliber teams in many different games.', '<strong>compLexity Gaming (coL)</strong> is a professional gaming organization based out of North America. Created in 2003, this organization has become a powerhouse in electronic sports competitions across multiple genre. compLexity Gaming is a member of the G7 teams.');
insert into teams values('', 'Immortals', 'immortals','Immortals.png', 1, '<strong>Immortals</strong> is a professional team from the United States.','<strong>Immortals</strong> is a venture capital-backed American eSports organization, primarily known for their League of Legends team. The ownership group includes a co-owner of the National Basketball Associations Memphis Grizzlies and the band Linkin Park (through their company Machine Shop Ventures) - among others. The organization is also active in Overwatch, Counter Strike: Global Offensive, and Smash. Following The International 2017 and the introduction of the new Majors and Minors, CEO Noah Winston announced his interest in signing a Dota 2 team.');
insert into teams values('', 'Pain Gaming', 'pain-gaming','pain.png', 2, '<strong>paiN Gaming</strong> is a Brazilian multi-gaming organization. They currently field teams in League of Legends, Vainglory", "Clash Royale, Overwatch and Dota 2.', '<strong>paiN Gaming</strong> was created in July 2010, as a DotA team from a merger between GameWise (Dunnaz, r22, Elai) and the old CNB lineup (PAADA, KINGRD, Sune, GrentxD). This lineup achieved some impressive results.

In August 2011, paiN switched to Dota 2 with their Dota 1 lineup, consisting of PAADA, KINGRD, Sune, ChiNa, GrentxD and Markeka.

In May 2013, paiN dropped their Dota 2 team and picked up a new Argentinian squad, which consisted of ddx, Zurdo, Ektoplazm, Fullback, and Gengar.

In June 2013, paiN picked up Meenix Esports (with players CHINA, Fuzzy, GHIZI, Ned, Sono, and Zoio) as their new Brazilian team.

In February 2014, paiN added 3 new players to their roster: Rain, NotPudga and bplay.');
insert into teams values('', 'Team Liquid', 'liquid','Liquid.png', 3, '<strong>Team Liquid (Liquid)</strong> is a professional gaming organization formed in 2000 and based in the Netherlands.', 'Team Liquid would come into The International 2017 as one of the favorites. For the Group Stage, they would be seeded into Group A alongside other favorites such as Evil Geniuses, LGD Gaming, or Team Secret among others. After a strong start to the Group Stage, Liquid found themselves in a neck-and-neck race with LGD Gaming for the top seed of their group, which Team Liquid finally were able to secure with an overall score of 13-3. This meant that they would be allowed to pick their opponent in the Upper Bracket of the Main Event from the 3rd and 4th placed teams from Group B: Virtus.pro or Invictus Gaming. Unwilling to face the aggressive Russian squad, Team Liquid chose iG as their first opponent.

However, the first series of the main event did not go as planned and Team Liquid dropped down to the Lower Bracket after losing 1-2 to iG. To make it to the Grand Finals, they would have to make a run similar to Digital Chaos''s run at The International 2016. Their first opponent in the Lower Bracket would be Team Secret, to whom they lost the first game of the series. Team Liquid found themselves one game away from elimination, but not for the last time at this event. Beating out the surprise contender Team Empire, fellow favorites Virtus.pro and LGD Gaming, Team Liquid managed to reach the Lower Bracket final, where they faced another surprise contender in LGD.Forever Young. LFY had managed to nearly sweep Group B, only dropping two games and hadn''t dropped a game on the main stage until the Upper Bracket Final against Newbee. LFY quickly proved themselves a formidable opponent and after the first game of the Lower Bracket final, Team Liquid found themselves once again with their backs against the wall. However, they prevailed yet again and turned the series around on the back of MinD_ContRoL''s Nature''s Prophet. Team Liquid had reached the Grand Final, where Newbee waited. Team Liquid had never dropped a series to Newbee and this time would be no different: Liquid swept the series 3-0 in dominant fashion, claiming their first Dota Major Championship and becoming the seventh winner of The International[15] and over 10 Million USD in prize money.

This would be the first time that a TI Grand Final would end in a 3-0 sweep as well as the first time a team made up of 5 different nationalities would win the Aegis. Newbees Faith, who was part of the Invictus Gaming roster that won The International 2012, was also denied his second TI win. Liquids victory also continued the tradition of the TI winner alternating between Chinese and Western teams.');

insert into teams values('', 'Natus Vincere', 'natus-vincere','NaVi.png', 4, '<strong>Natus Vincere</strong> (Latin: "born to win"), also known as <strong>Na`Vi</strong>, is a Ukrainian multigaming e-Sports organization. ', '<strong>Natus Vincere</strong> (Latin: "born to win"), also known as <strong>Na`Vi</strong>, is a Ukrainian multigaming e-Sports organization. ');
insert into teams values('', 'Newbee', 'newbee','Newbee.png', 5, '<strong>Newbee</strong> is a professional Dota 2 team based in Shanghai, China.', 'Newbee earned a direct invite to TI4 but struggled initially, finishing 7-8 in the second playoff phase before defeating LGD Gaming and Mousesports in tiebreakers for 9th place. However, they did not lose another series the rest of the way, first defeating Titan, Natus Vincere and Invictus Gaming to progress to the top 8 main event in the winners side. At the main event, they defeated Vici Gaming and then Evil Geniuses to set up an all-Chinese grand final against Vici Gaming - which was also a matchup of two teams who enjoy forming deathballs. VG took the first game, but Newbee came back to take the next three and the $5 million first prize, including a 19-3 blitzing in the decisive Game 4 in just over 15 minutes.');
insert into teams values('', 'Fnatic', 'fnatic','Fnatic.png', 6 , '<strong>Fnatic</strong> are a professional gaming team, consisting of players from across the globe who all make a living through competing in professional tournaments. ', 'One of the most adored teams in all of South East Asia, now is a time to breathe new life into our Dota 2 roster. Its hard to deny that while 2016 saw some euphoric times, Fnatic also suffered a difficult period towards the end of the year. Despite that, were determined to steer this ship back to the top of competitive play.

After three tremendously successful Major campaigns including a fourth place finish at The International, Fnatic Dota 2 will look to rediscover the form that saw them become so acclaimed in days gone by.');

insert into players values('', 'Syed Sumail Hassan', 'Suma1l', '1.jpg', 1, 'Offlane');
insert into players values('', 'Clinton Loomis', 'Fear', '2.jpg', 1, 'Mid');
insert into players values('', 'Artour Babaev', 'Arteezy', '4.jpg', 1, 'Carry');
insert into players values('', 'Rasmus Filipsen', 'Misery', '3.jpg', 1 , 'Support');
insert into players values('', 'Andreas Franck Nielsen', 'Cr1t', '5.jpg', 1, 'Support');

insert into teamImages values('', '11.jpg', 'sumail', 1);
insert into teamImages values('', '12.jpg', 'arteezy interview', 1);
insert into teamImages values('', '13.jpg', 'EG playing', 1);
insert into teamImages values('', '14.jpg', 'GESC vitory', 1);
