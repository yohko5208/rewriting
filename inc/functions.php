<?php
function findTeams(){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from teams"); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = array();
        while( $ligne = $resultats1->fetch() ) {
            $result[] = new Team($ligne->id, $ligne->name, $ligne->url, $ligne->image, $ligne->idRegion, $ligne->overview, $ligne->description);
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findTeamsByRegion($reg){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from teams where idRegion=".$reg); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = array();
        while( $ligne = $resultats1->fetch() ) {
            $result[] = new Team($ligne->id, $ligne->name, $ligne->url, $ligne->image, $ligne->idRegion, $ligne->overview, $ligne->description);
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findTeamsById($id){
    $connexion = getmysql();
    try{
        $query="select * from teams where id=".$id;
        $resultats1=$connexion->query($query); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = null;
        while( $ligne = $resultats1->fetch() ) {
            $result = new Team($ligne->id, $ligne->name, $ligne->url, $ligne->image, $ligne->idRegion, $ligne->overview, $ligne->description);
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findRegionById($id){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from regions where id=".$id); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = null;
        while( $ligne = $resultats1->fetch() ) {
            $result = $ligne->name;
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findRegionAbbrById($id){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from regions where id=".$id); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = null;
        while( $ligne = $resultats1->fetch() ) {
            $result = $ligne->short;
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findPlayersByTeam($id){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from players where idTeam=".$id); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = array();
        while( $ligne = $resultats1->fetch() ) {
            $result[]= new Player($ligne->id, $ligne->name, $ligne->nickname, $ligne->image, $ligne->idTeam, $ligne->role);
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}
function findImagesByTeam($id){
    $connexion = getmysql();
    try{
        $resultats1=$connexion->query("select * from teamImages where idTeam=".$id); // on va chercher tous les membres de la table qu'on trie par ordre croissant
        $resultats1->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet
        $result = array();
        while( $ligne = $resultats1->fetch() ) {
            $result[]= new TeamImages($ligne->id, $ligne->source, $ligne->alt, $ligne->idTeam);
        }
    }catch(PDOException $e){
        echo ($e->getMessage());
    }
    $connexion = null;
    return $result;
}