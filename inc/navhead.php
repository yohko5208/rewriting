<!-- Start Section Header style3 -->

<!--<section class="Header-Style3">
    <div class="TopHeader">
        <div class="container">
            <div class="row">
                <div class="Contact-h col-md-6">
                    <div class="PhoneNamber">
                        <p><i class="fa fa-phone"></i>+261 32 41 695 39</p>
                    </div>
                    <div class="Email-Site">
                        <p><i class="fa fa-envelope"></i>elisha5208@gmail.com</p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="Link-ul">
                        <ul class="icons-ul">
                            <li class="sidebar-menu"><a href="#" id="open-button" ><span>Login / Sign Up</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
    <!-- Start Navbar -->
<?php
    $tsNA = findTeamsByRegion(1);
    $tsSA = findTeamsByRegion(2);
    $tsEU = findTeamsByRegion(3);
    $tsCIS = findTeamsByRegion(4);
    $tsCN = findTeamsByRegion(5);
    $tsSEA = findTeamsByRegion(6);
?>
    <div class="Navbar-Header navbar basic" data-sticky="true">
        <div class="container">
            <div class="row">
                <div class="Logo-Header col-md-4">
                    <a class="navbar-logo" href="Home"></a>
                </div>
                <!--<div class="right-wrapper">
                    <div class="Icons-Search">
                        <a href="#"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="Block-Search">
                        <input id="m_search" name="s" type="text" placeholder="Type your search keyword here and hit Enter...">
                        <button><i class="fa fa-search"></i></button>
                    </div>
                </div>-->

                <div class="collapse pull-right navbar-collapse">
                    <div id="cssmenu" class="Menu-Header top-menu">
                        <ul>

                            <li class="<?php if(isset($home)&&$home==1){echo "current";} ?>"><a href="Home">Home</a></li>
                            <li class="has-sub yamm-fullwidth"><a href="#">Western Teams</a>
                                <ul>
                                    <li>
                                        <div class="col-md-3">
                                            <h5>North America (NA)</h5>
                                            <ul>
                                                <?php foreach($tsNA as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>

                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>South America (SA)</h5>
                                            <ul>
                                                <?php foreach($tsSA as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>Europe (EU)</h5>
                                            <ul>
                                                <?php foreach($tsEU as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>CommonWealth of Independant States (CIS)</h5>
                                            <ul>
                                                <?php foreach($tsCIS as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub yamm-fullwidth"><a href="#">Eastern Teams</a>
                                <ul>
                                    <li>
                                        <div class="col-md-6">
                                            <h5>China (CN)</h5>
                                            <ul>
                                                <?php foreach($tsCN as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>South-East Asia (SEA)</h5>
                                            <ul>
                                                <?php foreach($tsSEA as $t){ ?>
                                                    <li><a href="team-<?php echo $t->get_Id(); ?>-<?php echo $t->get_Url(); ?>"><?php echo $t->get_Name(); ?></a></li>
                                                <?php }
                                                ?>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!--<li class="has-sub"><a href="#">Portfolio</a>
                                <ul>
                                    <li><a href="#">Portfolio Classic</a>
                                        <ul class="sub-menu">
                                            <li><a href="../portfolio_classic_2col.html">Portfolio Classic 2Col</a></li>
                                            <li><a href="../portfolio_classic_3col.html">Portfolio Classic 3Col</a></li>
                                            <li><a href="../portfolio_classic_4col.html">Portfolio Classic 4Col</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Portfolio Masonry</a>
                                        <ul class="sub-menu">
                                            <li><a href="../portfolio_masonry_2col.html">Portfolio Masonry 2Col</a></li>
                                            <li><a href="../portfolio_masonry_3col.html">Portfolio Masonry 3Col</a></li>
                                            <li><a href="../portfolio_masonry_4col.html">Portfolio Masonry 4Col</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Portfolio Thumbnails</a>
                                        <ul class="sub-menu">
                                            <li><a href="../portfolio_style_1.html">Portfolio Style 1</a></li>
                                            <li><a href="../portfolio_style_2.html">Portfolio Style 2</a></li>
                                            <li><a href="../portfolio_style_3.html">Portfolio Style 3</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Portfolio Gallery</a>
                                        <ul class="sub-menu">
                                            <li><a href="../portfolio_gallery_2col.html">Portfolio Gallery 2Col</a></li>
                                            <li><a href="../portfolio_gallery_3col.html">Portfolio Gallery 3Col</a></li>
                                            <li><a href="../portfolio_gallery_4col.html">Portfolio Gallery 4Col</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Portfolio Fullwidth</a>
                                        <ul class="sub-menu">
                                            <li><a href="../portfolio_fullwidth_2col.html">Portfolio Fullwidth 2Col</a></li>
                                            <li><a href="../portfolio_fullwidth_3col.html">Portfolio Fullwidth 3Col</a></li>
                                            <li><a href="../portfolio_fullwidth_4col.html">Portfolio Fullwidth 4Col</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="../portfolio_single.html">Portfolio Single</a></li>
                                </ul>
                            </li>

                            <li class="has-sub"><a href="#">Blog</a>
                                <ul>
                                    <li><a href="#">Blog Grid</a>
                                        <ul class="sub-menu">
                                            <li><a href="../blog_grid_2col.html">Blog Grid 2Col</a></li>
                                            <li><a href="../blog_grid_3col.html">Blog Grid 3Col</a></li>
                                            <li><a href="../blog_grid_4col.html">Blog Grid 4Col</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Blog With Sidebar</a>
                                        <ul class="sub-menu">
                                            <li><a href="../blog_left_sidebar.html">Blog Left Sidebar</a></li>
                                            <li><a href="../blog_right_sidebar.html">Blog Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Blog Single</a>
                                        <ul class="sub-menu">
                                            <li><a href="../blog_single_centered.html">Blog Single Centered</a></li>
                                            <li><a href="../blog_single_left_sidebar.html">Blog Single Left Sidebar</a></li>
                                            <li><a href="../blog_single_right_sidebar.html">Blog Single Right Sidebar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-sub"><a href="#">Shop</a>
                                <ul>
                                    <li class="open-left"><a href="#">Shop Catalog List</a>
                                        <ul class="sub-menu">
                                            <li><a href="../shop_full_width.html">Full Width</a></li>
                                            <li><a href="../shop_grid_view.html">Grid View</a></li>
                                            <li><a href="../shop_list_view.html">List View</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="../shop_single_item.html">Shop Catalog Item</a></li>
                                    <li><a href="../shop_cart.html">Shop Cart Page</a></li>
                                    <li><a href="../shop_checkout.html">Checkout Page</a></li>
                                </ul>
                            </li>


                            <li class="has-sub yamm-fullwidth"><a href="#">Elements</a>
                                <ul>
                                    <li>
                                        <div class="col-md-6">
                                            <h5>Group 1</h5>
                                            <ul>
                                                <li><a href="../elements/accordions.html">Accordions<i class="fa fa-bars"></i></a></li>
                                                <li><a href="../elements/alert-box.html">Alert Boxes<i class="fa fa-exclamation"></i></a></li>
                                                <li><a href="../elements/animations.html">Animations<i class="fa fa-magic"></i></a></li>
                                                <li><a href="../elements/blockquotes.html">Block Quotes<i class="fa fa-quote-left"></i></a></li>
                                                <li><a href="../elements/buttons.html">Buttons<i class="fa fa-link"></i></a></li>
                                                <li><a href="../elements/counters.html">Counters<i class="fa fa-clock-o"></i></a></li>
                                                <li><a href="../elements/components.html">Components<i class="fa fa-play"></i></a></li>
                                                <li><a href="../elements/elements-forms.html">Forms<i class="fa fa-list"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <h5>Group 2</h5>
                                            <ul>
                                                <li><a href="../elements/features-boxes.html">Features Boxes<i class="fa fa-random"></i></a></li>
                                                <li><a href="../elements/maps.html">Maps<i class="fa fa-map-marker"></i></a></li>
                                                <li><a href="../elements/bars.html">Progress Bar<i class="fa fa-list-alt"></i></a></li>
                                                <li><a href="../elements/promo-boxes.html">Promo Boxes<i class="fa fa-rocket"></i></a></li>
                                                <li><a href="../elements/social-icons.html">Social Icons<i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="../elements/tabs.html">Tabs<i class="fa fa-star"></i></a></li>
                                                <li><a href="../elements/bootstrap-tables.html">Tables<i class="fa fa-table"></i></a></li>
                                                <li><a href="../elements/typography.html">Typography<i class="fa fa-text-height"></i></a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>-->

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End Navbar -->

</section>

<!-- End Section Header style3 -->