<script type="text/javascript" src="../assets/style/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../assets/style/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/style/js/menu-script.js"></script>

<script type="text/javascript" src="../assets/style/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="../assets/style/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

<script type="text/javascript" src="../assets/style/js/instafeed.min.js"></script>
<script type="text/javascript" src="../assets/style/js/rev_custom.js"></script>
<script type="text/javascript" src="../assets/style/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.inview.min.js"></script>
<script type="text/javascript" src="../assets/style/js/jflickrfeed.min.js"></script>
<script type="text/javascript" src="../assets/style/js/tweetie.min.js"></script>
<script type="text/javascript" src="../assets/style/js/instgram_custom.js"></script>

<script type="text/javascript" src="../assets/style/js/modernizr.custom.js"></script>
<script type="text/javascript" src="../assets/style/js/classie.js"></script>
<script type="text/javascript" src="../assets/style/js/main.js"></script>
<script type="text/javascript" src="../assets/style/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="../assets/style/js/jquery.counterup.js"></script>
<script type="text/javascript" src="../assets/style/js/waypoints.min.js"></script>
<script type="text/javascript" src="../assets/style/js/wow.min.js"></script>
<script type="text/javascript" src="../assets/style/js/custom.js"></script>