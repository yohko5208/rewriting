<?php

class Team
{
    private $_id;
    private $_name;
    private $_url;
    private $_image;
    private $_region;
    private $_overview;
    private $_description;

    public function __construct($id,$name,$u,$img,$reg,$ov,$desc){
        $this->_id = $id;
        $this->_name = $name;
        $this->_url=$u;
        $this->_image = $img;
        $this->_region = $reg;
        $this->_overview = $ov;
        $this->_description = $desc;
    }
    public function get_id(){
        return $this->_id;
    }
    public function get_Name(){
        return $this->_name;
    }
    public function get_Url(){
        return $this->_url;
    }
    public function get_Image(){
        return $this->_image;
    }
    public function get_Region(){
        return $this->_region;
    }
    public function get_Overview(){
        return $this->_overview;
    }
    public function get_Description(){
        return $this->_description;
    }


}