<?php

class Player
{
    private $_id;
    private $_name;
    private $_nickname;
    private $_image;
    private $_team;
    private $_role;

    public function __construct($id,$name,$nkname,$img,$t,$r){
        $this->_id = $id;
        $this->_name = $name;
        $this->_nickname = $nkname;
        $this->_image = $img;
        $this->_team = $t;
        $this->_role = $r;
    }
    public function get_id(){
        return $this->_id;
    }
    public function get_Name(){
        return $this->_name;
    }
    public function get_Image(){
        return $this->_image;
    }
    public function get_Nickname(){
        return $this->_nickname;
    }
    public function get_Team(){
        return $this->_team;
    }
    public function get_Role(){
        return $this->_role;
    }
}