<?php

class TeamImages
{
    private $_id;
    private $_source;
    private $_alt;
    private $_team;

    public function __construct($id,$src,$a,$t){
        $this->_id = $id;
        $this->_source = $src;
        $this->_alt = $a;
        $this->_team = $t;
    }
    public function get_id(){
        return $this->_id;
    }
    public function get_Source(){
        return $this->_source;
    }
    public function get_Alt(){
        return $this->_alt;
    }
    public function get_Team(){
        return $this->_team;
    }


}