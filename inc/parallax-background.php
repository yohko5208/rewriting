
<!-- Start Image Parallax Background  -->

<div class="scroll-section padd-vertical-0">
    <div class="tp-banner-container">
        <div id="rev_slider_100_1"  class="rev_slider fullwidthabanner" data-version="5.0.7">
            <ul>
                <li data-index="rs-12" data-transition="fade" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-description="">
                    <!-- MAIN IMAGE -->
                    <div style="background:rgba(0,0,0,0.6); opacity: 1;">
                        <img alt="" src="../assets/style/images/parallax/dota-2.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    </div>


                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Newspaper-Title-Centered tp-resizeme rs-parallaxlevel-0"
                         id="slide-3-layer-1"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-120','-120','-120','-120']"
                         data-fontsize="['50','50','50','40']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="auto:auto;s:1000;"
                         data-mask_in="x:0px;y:0px;"
                         data-mask_out="x:0;y:0;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"
                         data-elementdelay="0.05"

                         style="z-index: 5; white-space: nowrap;">Welcome to
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Newspaper-Title-Centered tp-resizeme rs-parallaxlevel-0"
                         id="slide-3-layer-2"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-50','-50','-50','-50']"
                         data-fontsize="['50','50','50','40']"
                         data-lineheight="['70','70','70','50']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 6; white-space: nowrap;">DOTAPEDIA
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
                         id="slide-3-layer-3"
                         data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['5','5','5','5']"
                         data-fontsize="['18','18','18','18']"
                         data-lineheight="['22','22','22','22']"
                         data-width="['','','','300']"
                         data-height="none"
                         data-whitespace="['nowrap','nowrap','nowrap','normal']"
                         data-transform_idle="o:1;"

                         data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                         data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                         data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                         data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                         data-start="1500"
                         data-splitin="none"
                         data-splitout="none"
                         data-responsive_offset="on"

                         style="z-index: 6; white-space: nowrap;">A website about everything you need to know on Dota professional teams!
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- End Image Parallax Background -->