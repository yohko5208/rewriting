<!-- Start Sidebar  -->

<div class="menu-wrap">
    <nav class="menu">
        <div class="TopSide-bar">
            <div class="Top-Block-Sidebar">
                <h2>Login</h2>

            </div>

            <div class="login-controls">
                <div class="input-box">
                    <input type="text" class="txt-box " placeholder="User name">
                </div>
                <div class="input-box">
                    <input type="password" class="txt-box " placeholder="Password">
                </div>
                <div class="main-bg">
                    <input type="submit" class="btn " value="Login">
                </div>
                <br/>
            </div>
            <div class="Sign-Up">
                <p><a href="#">Sign up now</a></p>
            </div>
        </div>
    </nav>
    <button class="close-button" id="close-button">Close Menu</button>
</div>


<!-- End Sidebar  -->